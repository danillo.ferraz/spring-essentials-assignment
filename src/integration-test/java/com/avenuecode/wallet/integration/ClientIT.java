package com.avenuecode.wallet.integration;

import com.avenuecode.wallet.dto.ClientBasicDTO;
import com.avenuecode.wallet.dto.ClientSignUpDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static com.avenuecode.wallet.controller.ClientControllerTest.asJsonString;
import static com.avenuecode.wallet.entity.ApplicationRoles.ROLE_CLIENT;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ClientIT extends IntegrationConfig {

    @Autowired
    private MockMvc mockMvc;

    /*this object will be used in the entire integration process*/
    private static ClientBasicDTO integrationClientDTO;


    @Test
    @Order(1)
    @DisplayName("INTEGRATION POST createClient as admin /api/v1/clients success")
    void createClient_asAdmin_givenValidRequest_shouldCreateClient() throws Exception {
        //Setup request
        ClientSignUpDTO request = new ClientSignUpDTO();
        request.setClientRole(ROLE_CLIENT);
        request.setClientName("Test Client");
        request.setPassword("password");
        request.setUsername("testclient");


        //Execute the request and assert
        MvcResult result = mockMvc.perform(post("/api/v1/clients/")
                .content(asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(user("admin")
                        .password("password")
                        .roles("ADMIN")))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.clientName").value("Test Client"))
                .andExpect(jsonPath("$.username").value("testclient"))
                .andExpect(jsonPath("$.walletList").isEmpty())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        integrationClientDTO = new ObjectMapper().readValue(result.getResponse().getContentAsString(), ClientBasicDTO.class);

    }

    @Test
    @Order(2)
    @DisplayName("INTEGRATION GET getClient as client /api/v1/clients/{id} success")
    void getClient_asClient_givenValidId_shouldReturnClientFromDatabase() throws Exception {

        //For the setup the test is using the brand new integrationClient created in the creation test

        //Execute the request + assert
        mockMvc.perform(get("/api/v1/clients/" + integrationClientDTO.getId())
                .with(user("client")
                        .password("password")
                        .roles("CLIENT")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.clientName").value("Test Client"))
                .andExpect(jsonPath("$.username").value("testclient"))
                .andExpect(jsonPath("$.walletList").isEmpty())
                .andDo(MockMvcResultHandlers.print());

    }

    @Test
    @Order(3)
    @DisplayName("INTEGRATION PUT updateClientName as admin /api/v1/clients/{id} success")
    void updateClientName_asAdmin_givenValidInput_shouldReturnUpdatedClientFromDatabase() throws Exception {
        //Setup request
        ClientBasicDTO updatedClient = new ClientBasicDTO();
        updatedClient.setClientName("new name");
        updatedClient.setId(integrationClientDTO.getId());

        //Execute + assert
        this.mockMvc.perform(
                put("/api/v1/clients/"+updatedClient.getId())
                        .content(asJsonString(updatedClient))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(user("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.clientName").value("new name"))
                .andExpect(jsonPath("$.username").value("testclient"))
                .andExpect(jsonPath("$.walletList").isEmpty())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    @Order(4)
    @DisplayName("INTEGRATION GET getAllClients as admin /api/v1/clients/ success")
    void getAllClients_asAdmin_shouldReturnListOfClients() throws Exception {

        //Execute the request + assert
        mockMvc.perform(get("/api/v1/clients/")
                .with(user("admin")
                        .password("password")
                        .roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isNotEmpty())
                .andDo(MockMvcResultHandlers.print());

    }

    @Test
    @Order(5)
    @DisplayName("INTEGRATION DELETE deleteClient as admin")
    void deleteClient_asAdmin_givenValidInput_shouldReturnOkStatus() throws Exception {

        //Execute the request + assert
        mockMvc.perform(delete("/api/v1/clients/"+integrationClientDTO.getId())
                .with(user("admin")
                        .password("password")
                        .roles("ADMIN")))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }


}
