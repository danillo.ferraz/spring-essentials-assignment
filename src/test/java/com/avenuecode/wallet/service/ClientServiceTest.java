package com.avenuecode.wallet.service;

import com.avenuecode.wallet.entity.Client;
import com.avenuecode.wallet.repository.ClientRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static com.avenuecode.wallet.entity.ApplicationRoles.ROLE_CLIENT;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ClientServiceTest {


    @InjectMocks
    private ClientService service;

    @Mock
    private ClientRepository repository;

    private static Client client;

    @BeforeAll
    static void init() {
        client = new Client();
        client.setId(UUID.randomUUID());
        client.setClientName("username");
        client.setPassword("$2a$10$gh/KnoNxCi1CTl3rstOwg.2vJAhzIdhx7siuBnADik9t4gE17If6a");
        client.setWalletList(new ArrayList<>());
        client.setClientRole(ROLE_CLIENT);
    }

    @Test
    @DisplayName("Test create client success")
    void createClient_whenInputIsValid_ReturnSavedClient() {
        //Setup mock repository
        doReturn(client).when(repository).save(any());

        //Execute the service call
        Client returnedClient = service.createClient(client);

        //Assert the response
        Assertions.assertNotNull(returnedClient, "The created client should not be null");
        Assertions.assertEquals(client.getClientName(), returnedClient.getClientName());
    }

    @Test
    @DisplayName("Test get client by UUID success")
    void getClientByUUID_whenValidInput_shouldReturnCorrectClient() {
        //Setup mock repository
        doReturn(Optional.of(client)).when(repository).findById(client.getId());

        //Execute service call
        Client returnedClient = service.getClientByUUID(client.getId());

        //Assert the response
        Assertions.assertNotNull(returnedClient, "The obtained client should not be null");
        Assertions.assertEquals(client.getId(), returnedClient.getId());

    }

    @Test
    @DisplayName("Test get client by UUID failed by inexistent UUID")
    void getClientByUUID_whenInvalidInput_shouldThrowException() {
        //Setup mock repository
        doThrow(NoSuchElementException.class).when(repository).findById(any());

        //Execute + assert
        NoSuchElementException thrown = assertThrows(
                NoSuchElementException.class,
                () -> service.getClientByUUID(UUID.randomUUID())
        );
    }

    @Test
    @DisplayName("Test delete client by UUID success")
    void deleteByUUID_whenValidInput_shouldNotThrowException() {
        //Setup mock repository
        doReturn(Optional.of(client)).when(repository).findById(client.getId());

        //Execute test
        service.deleteByUUID(client.getId());

        //Assert
        verify(repository).deleteById(client.getId());
    }

    @Test
    @DisplayName("Test delete client by UUID fails due invalid UUID")
    void deleteByUUID_whenInvalidInput_shouldThrowException() {
        //Setup mock repository
        doThrow(NoSuchElementException.class).when(repository).findById(any());

        //Execute + assert
        NoSuchElementException thrown = assertThrows(
                NoSuchElementException.class,
                () -> service.deleteByUUID(UUID.randomUUID())
        );
    }

    @Test
    @DisplayName("Test get all clients successfuly return list of clients")
    void getAllClients_whenThereIsClients_shouldReturnClientList() {
        //Setup mock repository
        List<Client> clientList = new ArrayList<>();
        clientList.add(client);
        doReturn(clientList).when(repository).findAll();

        //Execute
        List<Client> returned = service.getAllClients();

        //Assert
        Assertions.assertEquals(1, clientList.size());
        Assertions.assertEquals(client.getId(), clientList.iterator().next().getId());

    }

    @Test
    @DisplayName("Test get all clients returns empty list when there's nobody in the repository")
    void getAllClients_whenThereIsNoClient_shouldReturnEmptyList() {
        //Setup mock repository
        List<Client> clientList = new ArrayList<>();
        doReturn(clientList).when(repository).findAll();

        //Execute
        List<Client> returned = service.getAllClients();

        //Assert
        Assertions.assertEquals(0, clientList.size());
        Assertions.assertEquals(true, clientList.isEmpty());
    }

    @Test
    @DisplayName("Test update clientName should work when client exists")
    void updateClientName_whenClientExists_shouldUpdateClientName() {
        //Setup mock repository
        Client modified = new Client();
        modified.setClientName("modified");
        doReturn(Optional.of(client)).when(repository).findById(client.getId());
        doReturn(modified).when(repository).save(any());

        //Execute
        Client returned = service.updateClientName(client.getId(), client.getClientName());

        //Assert
        Assertions.assertNotEquals(client.getClientName(), returned.getClientName());

    }

    @Test
    @DisplayName("Test update clientName should throw exception if invalid client is given")
    void updateClientName_whenClientDoesNotExists_shouldThrowException() {
        //Setup mock repository
        doThrow(NoSuchElementException.class).when(repository).findById(any());

        //Execute + assert
        NoSuchElementException thrown = assertThrows(
                NoSuchElementException.class,
                () -> service.updateClientName(client.getId(), client.getClientName())
        );

    }


}
