package com.avenuecode.wallet.controller;

import com.avenuecode.wallet.entity.Client;
import com.avenuecode.wallet.service.ClientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import static com.avenuecode.wallet.entity.ApplicationRoles.ROLE_CLIENT;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;



@ExtendWith(SpringExtension.class)
@WebMvcTest(ClientController.class)
public class ClientControllerTest {

    @MockBean
    private ClientService service;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PasswordEncoder passwordEncoder;

    private Client client;
    private Client updatedClient;


    @BeforeEach
    void init() {
        client = new Client();
        client.setId(UUID.randomUUID());
        client.setClientName("Client");
        client.setUsername("client");
        client.setPassword("password");
        client.setWalletList(new ArrayList<>());
        client.setClientRole(ROLE_CLIENT);

        updatedClient = new Client();
        updatedClient.setId(UUID.randomUUID());
        updatedClient.setClientName("New Client Name");
        updatedClient.setUsername("client");
        updatedClient.setPassword("password");
        updatedClient.setWalletList(new ArrayList<>());
        updatedClient.setClientRole(ROLE_CLIENT);
    }

    @Test
    @DisplayName("GET as client /api/v1/clients/{id} success")
    void getClient_asClient_givenValidId_shouldReturnClient () throws Exception {
        //Setup mock service
        doReturn(client).when(service).getClientByUUID(client.getId());

        //Execute the request
        mockMvc.perform(get("/api/v1/clients/"+client.getId())
                .with(user("client")
                        .password("password")
                        .roles("CLIENT")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    @DisplayName("GET as admin /api/v1/clients/{id} success")
    void getClient_asAdmin_givenValidId_shouldReturnClient () throws Exception {
        //Setup mock service
        doReturn(client).when(service).getClientByUUID(client.getId());

        //Execute the request and assert
        mockMvc.perform(get("/api/v1/clients/"+client.getId())
                .with(user("admin")
                        .password("password")
                        .roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    @DisplayName("GET without credentials /api/v1/clients/{id} fails")
    void getClient_unauthenticated_shouldBeUnauthorized () throws Exception {
        //Setup mock service
        doReturn(client).when(service).getClientByUUID(client.getId());

        //Execute the request
        mockMvc.perform(get("/api/v1/clients/"+client.getId()))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("POST as admin /api/v1/clients success")
    void createClient_asAdmin_givenValidRequestBody_shouldReturnClient() throws Exception {
        when(service.createClient(any(Client.class)))
                .thenReturn(client);

        //Execute the request and assert
        mockMvc.perform(post("/api/v1/clients/")
                .content(asJsonString(client))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(user("admin")
                        .password("password")
                        .roles("ADMIN")))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    @DisplayName("POST as client /api/v1/clients fails due unauthorized status")
    void createClient_asClient_shouldReturnForbiddenStatusCode() throws Exception {

        //Execute and assert
        mockMvc.perform(post("/api/v1/clients/")
                .content(asJsonString(client))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(user("client").roles("CLIENT")))
                .andExpect(status().isUnauthorized());

    }

    @Test
    @DisplayName("DELETE as admin /api/v1/clients/{id} should be successful")
    void deleteClient_asAdmin_givenValidId_shouldBeSuccessful() throws Exception {
        //Execute and assert
        this.mockMvc.perform(
                delete("/api/v1/clients/"+client.getId())
                        .with(user("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("DELETE as client /api/v1/clients/{id} should be unauthorized")
    void deleteClient_asClient_shouldBeForbidden() throws Exception {
        //Execute and assert
        this.mockMvc.perform(
                delete("/api/v1/clients/"+client.getId())
                        .with(user("client").roles("CLIENT")))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("GET as Admin All clients /api/v1/clients successfully")
    void getAllClients_asAdmin_shouldReturnValidList() throws Exception {
        //Setup
        when(service.getAllClients())
                .thenReturn(Arrays.asList(client));

        //Execute and assert
        this.mockMvc.perform(
                get("/api/v1/clients/")
                        .with(user("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));

    }

    @Test
    @DisplayName("GET as client All clients /api/v1/clients should be unauthorized")
    void getAllClients_asClient_shouldBeForbidden() throws Exception {

        //Execute and assert
        this.mockMvc.perform(
                get("/api/v1/clients/")
                        .with(user("client").roles("CLIENT")))
                .andExpect(status().isUnauthorized());

    }

    @Test
    @DisplayName("PUT as admin update client name /api/v1/clients successfully")
    void updateClientName_asValidUser_givenValidId_shouldReturnUpdatedClient() throws Exception {
        //Setup
        when(service.updateClientName(any(UUID.class), any(String.class)))
                .thenReturn(updatedClient);

        //Execute and assert
        this.mockMvc.perform(
                put("/api/v1/clients/"+client.getId())
                        .content(asJsonString(client))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(user("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("clientName").value(updatedClient.getClientName()));
    }



    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
