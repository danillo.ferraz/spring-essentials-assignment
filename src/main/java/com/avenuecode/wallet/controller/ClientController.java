package com.avenuecode.wallet.controller;

import com.avenuecode.wallet.dto.ClientBasicDTO;
import com.avenuecode.wallet.dto.ClientSignUpDTO;
import com.avenuecode.wallet.entity.Client;
import com.avenuecode.wallet.security.IsAdmin;
import com.avenuecode.wallet.security.IsAdminOrClient;
import com.avenuecode.wallet.service.ClientService;
import com.avenuecode.wallet.util.ObjectMapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/clients")
@Slf4j
public class ClientController {

    private ClientService clientService;
    private PasswordEncoder passwordEncoder;

    public ClientController(ClientService clientService,
                            PasswordEncoder passwordEncoder) {
        this.clientService = clientService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping
    @IsAdmin
    public ResponseEntity<ClientBasicDTO> createClient(@RequestBody ClientSignUpDTO clientSignUpDTO) {

        log.info(String.format("request: %s", clientSignUpDTO.toString()));

        Client clientInput = ObjectMapperUtils.map(clientSignUpDTO, Client.class);
        clientInput.setPassword(passwordEncoder.encode(clientSignUpDTO.getPassword()));

        Client clientOutput = clientService.createClient(clientInput);
        ClientBasicDTO response = ObjectMapperUtils.map(clientOutput, ClientBasicDTO.class);

        log.info(String.format("response: %s", response.toString()));

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @GetMapping("/{id}")
    @IsAdminOrClient
    public ResponseEntity<ClientBasicDTO> getClient(@PathVariable UUID id) {
        log.info(String.format("request: %s", id));
        Client client = clientService.getClientByUUID(id);
        ClientBasicDTO response = ObjectMapperUtils.map(client, ClientBasicDTO.class);
        log.info(String.format("response: %s", response.toString()));
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @DeleteMapping("/{id}")
    @IsAdmin
    public ResponseEntity<?> deleteClient(@PathVariable UUID id) {
        log.info(String.format("request: %s", id));
        clientService.deleteByUUID(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping
    @IsAdmin
    public ResponseEntity<List<ClientBasicDTO>> getAllClients() {
        List<Client> clients = clientService.getAllClients();
        List<ClientBasicDTO> response = clients
                .stream()
                .map(c -> ObjectMapperUtils.map(c, ClientBasicDTO.class))
                .collect(Collectors.toList());
        log.info("response: " + response.toString());
        return ResponseEntity.status(HttpStatus.OK)
                .body(response);
    }

    @PutMapping("{id}")
    @IsAdminOrClient
    public ResponseEntity<ClientBasicDTO> updateClientName(@PathVariable UUID id, @RequestBody ClientBasicDTO clientBasicDTO) {
        log.info(String.format("request: %s", clientBasicDTO.toString()));
        Client client = clientService.
                updateClientName(id, clientBasicDTO.getClientName());
        ClientBasicDTO response = ObjectMapperUtils.map(client, ClientBasicDTO.class);
        log.info("response: " + response.toString());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


}
