package com.avenuecode.wallet.controller;

import com.avenuecode.wallet.dto.WalletBasicDTO;
import com.avenuecode.wallet.dto.WalletSummaryDTO;
import com.avenuecode.wallet.entity.Wallet;
import com.avenuecode.wallet.security.IsAdmin;
import com.avenuecode.wallet.security.IsAdminOrClient;
import com.avenuecode.wallet.security.IsClient;
import com.avenuecode.wallet.service.WalletService;
import com.avenuecode.wallet.util.ObjectMapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/clients/{id}/wallets")
@Slf4j
public class WalletController {

    private WalletService walletService;

    public WalletController(WalletService walletService) {
        this.walletService = walletService;
    }

    @PostMapping
    @IsClient
    public ResponseEntity<WalletBasicDTO> createWallet(
            @PathVariable("id") UUID clientId, @RequestBody WalletBasicDTO dto) {
        log.info(String.format("request: %s, %s", clientId, dto));
        Wallet wallet = ObjectMapperUtils.map(dto, Wallet.class);
        walletService.createWallet(clientId, wallet);
        WalletBasicDTO response = ObjectMapperUtils.map(wallet, WalletBasicDTO.class);
        log.info(String.format("response: %s", response));
        return ResponseEntity.status(HttpStatus.CREATED).body(response);

    }

    @GetMapping
    @IsAdminOrClient
    public ResponseEntity<List<WalletBasicDTO>> getWalletsByClientId(@PathVariable("id") UUID clientId) throws Exception {
        log.info(String.format("request: %s", clientId));
        List<Wallet> walletList = walletService.getAllByClientId(clientId);
        List<WalletBasicDTO> response = ObjectMapperUtils.mapAll(walletList, WalletBasicDTO.class);
        log.info(String.format("response: %s", response));
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping("/{walletId}")
    @IsAdminOrClient
    public ResponseEntity<WalletSummaryDTO> getWalletBalance(@PathVariable("walletId") UUID walletId) {
        log.info(String.format("request: %s", walletId));
        Wallet wallet = walletService.getWalletByUUID(walletId);
        WalletSummaryDTO response = ObjectMapperUtils.map(wallet, WalletSummaryDTO.class);
        log.info(String.format("response: %s", response));
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @DeleteMapping("/{walletId}")
    @IsClient
    public ResponseEntity<WalletBasicDTO> deleteWallet(@PathVariable("walletId") UUID walletId) throws Exception {
        log.info(String.format("request: %s", walletId));
        Wallet wallet = walletService.deleteWalletByUUID(walletId);
        WalletBasicDTO response = ObjectMapperUtils.map(wallet, WalletBasicDTO.class);
        log.info(String.format("response: %s", response));
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
