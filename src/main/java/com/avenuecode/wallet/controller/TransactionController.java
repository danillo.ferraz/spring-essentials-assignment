package com.avenuecode.wallet.controller;

import com.avenuecode.wallet.dto.TransactionDTO;
import com.avenuecode.wallet.entity.Transaction;
import com.avenuecode.wallet.security.IsClient;
import com.avenuecode.wallet.service.TransactionService;
import com.avenuecode.wallet.util.ObjectMapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/transactions")
@Slf4j
public class TransactionController {

    private TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping("/")
    @IsClient
    public ResponseEntity<TransactionDTO> createTransaction(
            @RequestBody TransactionDTO dto) {
        log.info(String.format("request: %s", dto));
        Transaction input = ObjectMapperUtils.map(dto, Transaction.class);
        Transaction output = transactionService.createTransaction(input, dto.getAssetId(), dto.getWalletId());
        TransactionDTO response = ObjectMapperUtils.map(output, TransactionDTO.class);
        log.info(String.format("response: %s", response));
        return ResponseEntity.status(HttpStatus.CREATED).body(response);

    }

}
