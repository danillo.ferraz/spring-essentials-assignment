package com.avenuecode.wallet.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Transient;
import java.math.BigDecimal;

@Data
public class AssetBalance extends Asset{

    @Transient
    BigDecimal balance;

    public AssetBalance(Asset asset, BigDecimal balance) {
        super(asset.getId(), asset.getAssetName(), asset.getAbbreviation());
        this.balance = balance;
    }
}
