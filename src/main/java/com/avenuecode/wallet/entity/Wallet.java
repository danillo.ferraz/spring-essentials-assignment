package com.avenuecode.wallet.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Slf4j
@Table(name = "wallet")
public class Wallet extends EntityWithUUID {

    private String walletName;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private Client client;
    @OneToMany(mappedBy = "wallet",  fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Transaction> transactionList;

    @Transient
    public List<AssetBalance> getAssetBalanceList() {

        Map<Asset, List<Transaction>> transactionsByAsset = getAssetListTransactionsMap();
        log.debug(String.format("group all transactions by Asset: %s", transactionsByAsset));

        Map<Asset, BigDecimal> assetAmountHashMap = getAssetAmountMap(transactionsByAsset);
        log.debug(String.format("subtotal of Deposits (sum) and Withdrawals (subtract) by Asset", assetAmountHashMap));

        List<AssetBalance> assetBalanceList = getAssetBalanceList(assetAmountHashMap);
        log.debug(String.format("converting the map into a list of AssetBalance", assetBalanceList));

        return assetBalanceList;
    }

    private List<AssetBalance> getAssetBalanceList(Map<Asset, BigDecimal> assetAmountHashMap) {
        List<AssetBalance> assetBalanceList = new ArrayList<>();
        assetAmountHashMap.forEach(
                (asset, amount) -> {
                    assetBalanceList.add(new AssetBalance(asset, amount));
                }
        );
        return assetBalanceList;
    }

    private Map<Asset, BigDecimal> getAssetAmountMap(Map<Asset, List<Transaction>> transactionsByAsset) {
        Map<Asset, BigDecimal> assetAmountHashMap = new HashMap<>();
        transactionsByAsset.forEach((asset, transactions) -> {
            assetAmountHashMap.put(asset, transactions
                    .stream()
                    .map(t -> t.getOperationType().equals(OperationType.WITHDRAW) ? t.getAmount().multiply(BigDecimal.valueOf(-1)) : t.getAmount())
                    .reduce(BigDecimal.ZERO, BigDecimal::add));
        });
        return assetAmountHashMap;
    }

    private Map<Asset, List<Transaction>> getAssetListTransactionsMap() {
        Map<Asset, List<Transaction>> transactionsByAsset = transactionList
                .stream()
                .collect(
                        Collectors.groupingBy(Transaction::getAsset));
        return transactionsByAsset;
    }
}

