package com.avenuecode.wallet.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
@Getter
@Setter
public class EntityWithUUID {
    @Id
    @Type(type = "pg-uuid")
    @GeneratedValue
    private UUID id;

    public EntityWithUUID() {
        this.id = UUID.randomUUID();
    }
    public EntityWithUUID(UUID id) {this.id = id; }
}
