package com.avenuecode.wallet.entity;

public enum ApplicationRoles {
    ROLE_CLIENT,
    ROLE_ADMIN;
}
