package com.avenuecode.wallet.entity;

public enum OperationType {
    DEPOSIT,
    WITHDRAW
}
