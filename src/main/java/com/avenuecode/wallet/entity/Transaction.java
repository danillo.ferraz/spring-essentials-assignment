package com.avenuecode.wallet.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="transaction")
public class Transaction extends EntityWithUUID{

    @ManyToOne
    @JoinColumn(name="wallet_id")
    private Wallet wallet;
    @ManyToOne
    @JoinColumn(name="asset_id")
    private Asset asset;
    private BigDecimal amount;
    @Enumerated(EnumType.STRING)
    @Column(name = "op", length = 8)
    private OperationType operationType;

    @Transient
    public String getAssetAbbreviation() {
        return this.asset.getAbbreviation();
    }

    @Transient
    public UUID getWalletId() {
        return this.getAsset().getId();
    }
    @Transient
    public UUID getAssetId() {
        return this.getWallet().getId();
    }

}
