package com.avenuecode.wallet.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="asset")
public class Asset extends EntityWithUUID {

    private String assetName;
    private String abbreviation;
    @OneToMany(mappedBy = "asset",  fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Transaction> transactionList;

    public Asset(UUID id,String assetName, String abbreviation) {
        super(id);
        this.assetName = assetName;
        this.abbreviation = abbreviation;
    }

}
