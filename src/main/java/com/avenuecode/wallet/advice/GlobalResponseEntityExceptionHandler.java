package com.avenuecode.wallet.advice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@ControllerAdvice
public class GlobalResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<Object> handleCityNotFoundException(
            NoSuchElementException ex, WebRequest request) {
        log.error(String.format("NoSuchElementException thrown, %s", ex.getLocalizedMessage()));
        Map<String, Object> body = new LinkedHashMap<>();
        populateBody(request, body, "The element you're looking for does not exist.");
        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handleAccessDeniedException(
            AccessDeniedException ex, WebRequest request) {
        log.error(String.format("AccessDeniedException thrown, %s", ex.getLocalizedMessage()));
        Map<String, Object> body = new LinkedHashMap<>();
        populateBody(request, body, "You must authenticate to access this resource.");
        return new ResponseEntity<>(body, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<Object> handleDataIntegrityViolationException(
            DataIntegrityViolationException ex, WebRequest request) {
        log.error(String.format("DataIntegrityViolationException thrown, %s", ex.getLocalizedMessage()));
        Map<String, Object> body = new LinkedHashMap<>();
        populateBody(request, body, "The resource you're trying to add or modify violates a constraint.");
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request)
    {
        log.error(String.format("DataIntegrityViolationException thrown, %s", ex.getLocalizedMessage()));
        Map<String, Object> body = new LinkedHashMap<>();
        populateBody(request, body, ex.getMessage());
        return new ResponseEntity(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private void populateBody(WebRequest request, Map<String, Object> body, String message) {
        body.put("timestamp", LocalDateTime.now());
        body.put("message", message);
        body.put("description", request.getDescription(false));
    }
}
