package com.avenuecode.wallet.service;

import com.avenuecode.wallet.entity.Transaction;
import com.avenuecode.wallet.repository.AssetRepository;
import com.avenuecode.wallet.repository.TransactionRepository;
import com.avenuecode.wallet.repository.WalletRepository;
import com.avenuecode.wallet.security.IsAdminOrClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class TransactionService {

    private TransactionRepository transactionRepository;
    private AssetRepository assetRepository;
    private WalletRepository walletRepository;

    public TransactionService(
            TransactionRepository transactionRepository,
            AssetRepository assetRepository,
            WalletRepository walletRepository) {
        this.transactionRepository = transactionRepository;
        this.assetRepository = assetRepository;
        this.walletRepository = walletRepository;
    }


    @IsAdminOrClient
    public Transaction createTransaction(Transaction t, UUID assetId, UUID walletId) {
        t.setAsset(assetRepository
                .findById(assetId)
                .orElseThrow());
        t.setWallet(walletRepository
                .findById(walletId)
                .orElseThrow());
        return transactionRepository.save(t);
    }
}
