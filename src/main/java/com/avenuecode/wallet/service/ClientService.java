package com.avenuecode.wallet.service;

import com.avenuecode.wallet.entity.Client;
import com.avenuecode.wallet.repository.ClientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
@Slf4j
public class ClientService implements UserDetailsService{

    private ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository){
        this.clientRepository = clientRepository;
    }

    public Client createClient(Client client){
        return clientRepository.save(client);
    }

    public Client getClientByUUID(UUID id) {
        return clientRepository.findById(id).orElseThrow();
    }

    public void deleteByUUID(UUID id) {
        clientRepository.findById(id).ifPresentOrElse(
                c -> clientRepository.deleteById(c.getId()),
                () -> new NoSuchElementException());
    }

    public List<Client> getAllClients() {
        List<Client> clients = new ArrayList<>();
        clientRepository.findAll().forEach(clients::add);
        return clients;
    }

    public Client updateClientName(UUID id, String clientName) {
        var updated = clientRepository.findById(id).orElseThrow();
        updated.setClientName(clientName);
        return clientRepository.save(updated);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return clientRepository.findByUsername(username);
    }
}
