package com.avenuecode.wallet.service;

import com.avenuecode.wallet.dto.WalletBasicDTO;
import com.avenuecode.wallet.entity.Client;
import com.avenuecode.wallet.entity.Wallet;
import com.avenuecode.wallet.repository.ClientRepository;
import com.avenuecode.wallet.repository.WalletRepository;
import com.avenuecode.wallet.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class WalletService {

    private WalletRepository walletRepository;
    private ClientRepository clientRepository;

    public WalletService(WalletRepository walletRepository, ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
        this.walletRepository = walletRepository;
    }

    public Wallet createWallet(UUID clientId, Wallet wallet) {
        return clientRepository.findById(clientId).map(
                client -> {
                    wallet.setClient(client);
                    return walletRepository.save(wallet);
                }
        ).orElseThrow();
    }

    public List<Wallet> getAllByClientId(UUID clientId) throws Exception {
        return clientRepository.findById(clientId).map(
                client -> { return walletRepository.findByClient(client).orElseThrow();}
        ).orElseThrow();

    }

    public Wallet getWalletByUUID(UUID walletId) {
        return walletRepository.findById(walletId).orElseThrow();
    }

    public Wallet deleteWalletByUUID(UUID walletId) throws Exception {
        return walletRepository.findById(walletId).map(
                wallet -> {
                    walletRepository.deleteById(walletId);
                    return wallet;
                }
        ).orElseThrow();

    }
}
