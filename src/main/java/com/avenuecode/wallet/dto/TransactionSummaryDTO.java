package com.avenuecode.wallet.dto;

import com.avenuecode.wallet.entity.OperationType;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransactionSummaryDTO {

    private String assetAbbreviation;
    private BigDecimal amount;
    private OperationType operationType;

}

