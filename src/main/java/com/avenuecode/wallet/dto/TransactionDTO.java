package com.avenuecode.wallet.dto;

import com.avenuecode.wallet.entity.OperationType;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
public class TransactionDTO {

    private UUID walletId;
    private UUID assetId;
    private BigDecimal amount;
    private OperationType operationType;

}
