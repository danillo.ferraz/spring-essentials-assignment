package com.avenuecode.wallet.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class WalletBasicDTO {

    private UUID id;
    private String walletName;

}
