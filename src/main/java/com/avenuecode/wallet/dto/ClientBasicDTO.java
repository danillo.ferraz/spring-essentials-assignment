package com.avenuecode.wallet.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class ClientBasicDTO {

    private UUID id;
    private String username;
    private String clientName;
    private List<WalletBasicDTO> walletList = new ArrayList<>();


}
