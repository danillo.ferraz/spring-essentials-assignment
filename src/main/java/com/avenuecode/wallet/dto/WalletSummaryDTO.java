package com.avenuecode.wallet.dto;

import lombok.Data;

import java.util.List;

@Data
public class WalletSummaryDTO extends WalletBasicDTO {

    private List<AssetBalanceDTO> assetBalanceList;

}
