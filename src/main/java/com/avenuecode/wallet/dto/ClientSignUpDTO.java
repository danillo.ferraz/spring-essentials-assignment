package com.avenuecode.wallet.dto;

import com.avenuecode.wallet.entity.ApplicationRoles;
import lombok.Data;

@Data
public class ClientSignUpDTO extends ClientBasicDTO {

    private ApplicationRoles clientRole;
    private String password;
}
