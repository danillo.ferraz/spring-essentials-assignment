package com.avenuecode.wallet.dto;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class AssetBalanceDTO {

    private String assetName;
    private String abbreviation;
    private BigDecimal balance;

}
