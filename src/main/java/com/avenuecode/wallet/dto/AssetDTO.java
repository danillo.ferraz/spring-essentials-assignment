package com.avenuecode.wallet.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class AssetDTO {

    private UUID id;
    private String assetName;
    private String abbreviation;
}
