package com.avenuecode.wallet.repository;

import com.avenuecode.wallet.entity.Client;
import com.avenuecode.wallet.entity.Wallet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface WalletRepository extends CrudRepository<Wallet, UUID> {

    Optional<List<Wallet>> findByClient(Client client);
}
