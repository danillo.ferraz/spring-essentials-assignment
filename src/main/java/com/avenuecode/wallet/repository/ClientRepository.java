package com.avenuecode.wallet.repository;

import com.avenuecode.wallet.entity.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ClientRepository extends CrudRepository<Client, UUID> {

    Client findByUsername(String username);

    boolean existsClientByUsernameAndClientRole(String username, String clientRole);
}
