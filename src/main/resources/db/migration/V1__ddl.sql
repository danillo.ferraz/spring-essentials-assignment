SELECT 'CREATE DATABASE spring_assignment_db' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'spring_assignment_db');

DROP TABLE IF EXISTS transaction CASCADE;
DROP TABLE IF EXISTS wallet;
DROP TABLE IF EXISTS asset;
DROP TABLE IF EXISTS client;

CREATE TABLE client (
  id UUID NOT NULL,
  username VARCHAR(20) UNIQUE NOT NULL,
  password VARCHAR(60) NOT NULL,
  client_name VARCHAR(50) NOT NULL,
  client_role VARCHAR(20) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE wallet (
  id UUID NOT NULL,
  client_id UUID NOT NULL,
  wallet_name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE asset (
  id UUID NOT NULL,
  asset_name VARCHAR(50) NOT NULL,
  abbreviation VARCHAR(4) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE transaction (
  id UUID NOT NULL,
  wallet_id UUID NOT NULL,
  asset_id UUID NOT NULL,
  amount DECIMAL(12,2) NOT NULL,
  op VARCHAR(8) NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE wallet
ADD CONSTRAINT fk_wallet_client
FOREIGN KEY (client_id)
REFERENCES client(id);

ALTER TABLE transaction
ADD CONSTRAINT fk_transaction_wallet
FOREIGN KEY (wallet_id)
REFERENCES wallet(id);

ALTER TABLE transaction
ADD CONSTRAINT fk_transaction_asset
FOREIGN KEY (asset_id)
REFERENCES asset(id);

