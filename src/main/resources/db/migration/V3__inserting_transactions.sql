INSERT INTO transaction (id, wallet_id, asset_id, amount, op)
VALUES ('0a724c86-c5e0-448f-9fae-cd8ae6088594',
        '0273464d-8048-49f5-a162-29fc5375e75d',
        '4eb7a4a0-b9ae-11eb-8529-0242ac130003',
        10,
        'DEPOSIT');

INSERT INTO transaction (id, wallet_id, asset_id, amount, op)
VALUES ('e342e362-4883-4d8c-b1be-e68d4ebe37a3',
        '0273464d-8048-49f5-a162-29fc5375e75d',
        '4eb7a4a0-b9ae-11eb-8529-0242ac130003',
        5,
        'DEPOSIT');

INSERT INTO transaction (id, wallet_id, asset_id, amount, op)
VALUES ('d7bd9116-6a25-4531-9d82-2230b5a5082e',
        '0273464d-8048-49f5-a162-29fc5375e75d',
        '4eb7a4a0-b9ae-11eb-8529-0242ac130003',
        6,
        'WITHDRAW');

INSERT INTO transaction (id, wallet_id, asset_id, amount, op)
VALUES ('348c4b58-292e-4728-9d6a-2a2e71df0667',
        '0273464d-8048-49f5-a162-29fc5375e75d',
        '5fa3b2d8-ae61-49ee-aa19-8025db4295c6',
        20,
        'DEPOSIT');

INSERT INTO transaction (id, wallet_id, asset_id, amount, op)
VALUES ('0ddaa5f0-e3f3-49d7-be12-687798f1cfa6',
        '0273464d-8048-49f5-a162-29fc5375e75d',
        '5fa3b2d8-ae61-49ee-aa19-8025db4295c6',
        21,
        'WITHDRAW');
