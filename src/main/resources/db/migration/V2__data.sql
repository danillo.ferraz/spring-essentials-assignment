INSERT INTO asset (id, asset_name, abbreviation)
VALUES ('4eb7a4a0-b9ae-11eb-8529-0242ac130003', 'Bitcoin', 'BTC');
INSERT INTO asset (id, asset_name, abbreviation)
VALUES ('195fc23b-cd54-455c-8489-cff1170f2c7f', 'USDollar', 'USD');
INSERT INTO asset (id, asset_name, abbreviation)
VALUES ('5fa3b2d8-ae61-49ee-aa19-8025db4295c6', 'Cardano', 'ADA');
INSERT INTO asset (id, asset_name, abbreviation)
VALUES ('6384e3c4-58c0-4952-b0e0-1afbb68dd837', 'Etherium', 'ETH');
INSERT INTO asset (id, asset_name, abbreviation)
VALUES ('8124dbdc-26aa-4f16-8360-ba511c7df91a', 'Dogecoin', 'DOGE');


INSERT into client(id, username, password, client_name, client_role)
values (
    '5d59fcaa-673f-4d8c-b03e-a6f637145b79',
    'admin',
    '$2a$10$gh/KnoNxCi1CTl3rstOwg.2vJAhzIdhx7siuBnADik9t4gE17If6a',
    'Admin',
    'ROLE_ADMIN'
    );

INSERT into client(id, username, password, client_name, client_role)
values (
    '56bac5d8-eb50-4956-a5c8-f29c20dcf1bc',
    'danillo',
    '$2a$10$gh/KnoNxCi1CTl3rstOwg.2vJAhzIdhx7siuBnADik9t4gE17If6a',
    'Danillo Ferraz',
    'ROLE_CLIENT'
    );

INSERT into wallet(id, client_id, wallet_name)
values ('0273464d-8048-49f5-a162-29fc5375e75d','5d59fcaa-673f-4d8c-b03e-a6f637145b79', 'Wallet Test');