CREATE DATABASE spring_assignment_db;
CREATE USER postgres WITH PASSWORD 'mysecretpassword';
GRANT ALL PRIVILEGES ON DATABASE "my_database" to postgres;