# ACP2P Java Essentials
## Week 3 & 4 - Spring Essentials Assignment
### Wallet Application

How it works:

This is a simple application in Spring Boot that allows users to create personal wallets to store their currencies in.

Before explaining how to use it, I will introduce some technical features that are mandatory for the assignment:

- Lombok is being used mainly with **Data** and **Slf4j** logging;
- Project was made following standards for Java and Spring applications, such as division in layers of responsability;
- The application is secured with Basic authentication, but also has two types of authorizations, ADMIN and CLIENT auths;
- The application runs in a dockerized environment, using a docker-compose.yml file for bootstrapping;
- The application must be connected to a Postgres database, running on a separate Docker container;
- The application logs the interaction it receives via its endpoints with Slf4j with logback;
- There are unit and integration tests for the application. Unit and Integration Tests are implemented in separate modules. (Bonus: both tests runs in separate stages in the GitLab pipeline accordingly);
- Flyway was used to create tables and store some data.


This is the ERD:
![ERD](https://gitlab.com/danillo.ferraz/spring-essentials-assignment/-/raw/master/images/ERD.png)

### To bootstrap the app do the following:

```bash
./mvnw clean package -DskipTests
docker-compose up
```

All endpoints are secured and there are two types of authorities, ADMIN and CLIENT:

#### CLIENT ENDPOINTS

- create a client (admins only)
- get a client (admins and clients)
- delete a client (admins only)
- get all clients (admins only)
- update a client's name (admins and clients)

#### WALLET ENDPOINTS

- create a wallet (clients only)
- get all client's wallets (admins and clients)
- get the wallet balance summary (admins and clients)
- delete a wallet (clients only)

#### TRANSACTION ENDPOINT

- create a transaction (clients only)


All endpoints are registered in the postman collection in the `wallet-app.postman_collection.json` file in the root of this project.

The database starts with two clients:

| user  | password   | role  |
| ------------ | ------------ | ------------ |
| admin  | password   | ADMIN  |
| danillo  | password  | CLIENT  |


With them it is possible to access all endpoints and start playing around

To add transactions I will append a list of Asset IDs bellow for reference:

| Asset  | ID   |
| ------------ | ------------ |
| Bitcoin  | 4eb7a4a0-b9ae-11eb-8529-0242ac130003  |
| USDollar  | 195fc23b-cd54-455c-8489-cff1170f2c7f  |
| Cardano  | 5fa3b2d8-ae61-49ee-aa19-8025db4295c6  |
| Etherium  |  6384e3c4-58c0-4952-b0e0-1afbb68dd837 |
| Dogecoin  |  8124dbdc-26aa-4f16-8360-ba511c7df91a |


Enjoy!




